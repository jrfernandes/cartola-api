package com.analistajunior.cartola.model;

public enum Permissao {
	
	ADMIN("Administrador"),
	JOGADOR("Jogador");
	
	private String descricao;
	
	Permissao(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
}
