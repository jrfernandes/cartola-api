package com.analistajunior.cartola.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.analistajunior.cartola.model.Jogador;
import com.analistajunior.cartola.model.Pontuacao;
import com.analistajunior.cartola.repository.JogadorRepository;
import com.analistajunior.cartola.repository.PontuacaoRepository;
import com.analistajunior.cartola.service.exceptions.DataIntegrityException;
import com.analistajunior.cartola.service.exceptions.ObjectNotFountException;

@Service
public class JogadorService {

	@Autowired
	private JogadorRepository repository;
	
	@Autowired
	private PontuacaoRepository pontuacaoRepository;

	public Jogador salvar(Jogador jogador) {
		if (repository.findByTime(jogador.getTime()) == null && repository.findByLogin(jogador.getLogin()) == null)
			return repository.save(jogador);
		else {
			throw new DataIntegrityException("Time ou login já cadastrado.");
		}

	}

	public Jogador buscarPorTime(String time) {
		if (repository.findByTime(time) == null) {
			throw new ObjectNotFountException("Time não encontrado.");
		}
		Jogador jogador = repository.findByTime(time);
		return pontuacaoDoJogador(jogador);
	}
	
	public Jogador buscarPorId(Long id) {
		if(repository.findOne(id) == null) {
			throw new ObjectNotFountException("Time não encontrado.");
		}
		Jogador jogador = repository.findOne(id);
		return pontuacaoDoJogador(jogador);
	}
	
	public Jogador buscarPorLogin(String login) {
		if(repository.findByLogin(login) ==  null) {
			throw new ObjectNotFountException("Login não encontrado");
		}
		return repository.findByLogin(login);
	}

	public Jogador adicionarPontuacao(Long id, double pontuacao) {
		Jogador jogador = repository.findOne(id);
		jogador.setPontuacao(jogador.getPontuacao() + pontuacao);
		return repository.save(jogador);
	}

	public Jogador atualizarPontuacao(Long id, double pontuacao) {
		Jogador jogador = repository.findOne(id);
		jogador.setPontuacao(pontuacao);
		return repository.save(jogador);
	}

	public void deletarJogador(Long id) {
		buscarPorId(id);
		repository.delete(id);
	}
	
	
	private Jogador pontuacaoDoJogador(Jogador jogador) {
		jogador.setPontuacao(pontuacaoRepository.pontuacaoPorJogador(jogador));
		return jogador;
	}
	
	private Jogador pontuacaoPorRodada(Jogador jogador, int rodada) {
		jogador.setPontuacao(pontuacaoRepository.pontuacaoJogadorPorRodada(jogador, rodada));
		return jogador;
	}
	
	private void zerarPontuacao() {
		List<Jogador> jogadores = repository.findAll();
		for(Jogador j : jogadores) {
			atualizarPontuacao(j.getId(), 0);
		}
	}

	public List<Jogador> classificacaoGeral(){
		zerarPontuacao();
		List<Jogador> jogadores = repository.findAll();
		for(Jogador j : jogadores) {
			if(j.getPontuacao() != pontuacaoDoJogador(j).getPontuacao()) {
				j = pontuacaoDoJogador(j);
				atualizarPontuacao(j.getId(), j.getPontuacao());
			}
		}
		
		return repository.classificacao();
	}
	
	public List<Jogador> classificacaoRodada(int rodada){
		zerarPontuacao();
		if(pontuacaoRepository.findByRodada(rodada).isEmpty()) {
			throw new ObjectNotFountException("Rodada não cadastrada.");
		}
		List<Pontuacao> pontuacoes = pontuacaoRepository.findByRodada(rodada);
		List<Jogador> jogadores = new ArrayList<>();
		for(Pontuacao p : pontuacoes) {
			jogadores.add(repository.findOne(p.getJogador().getId()));
		}
		for(Jogador j : jogadores) {
			if(j.getPontuacao() != pontuacaoPorRodada(j, rodada).getPontuacao()) {
				j = pontuacaoPorRodada(j, rodada);
				atualizarPontuacao(j.getId(), j.getPontuacao());
			}
		}
		return repository.classificacao();
	}
	
	@SuppressWarnings("deprecation")
	public List<Jogador> classificacaoMes(int mes){
		zerarPontuacao();
		List<Jogador> jogadores = repository.findAll();
		
		for(Jogador j : jogadores) {
			List<Pontuacao> pontuacoes = j.getPontuacoes();
			for(Pontuacao p: pontuacoes) {
				if(p.getData().getMonth() == mes-1) {
					j.setPontuacao(j.getPontuacao()+p.getPontos());
				}
			}
		}
		return jogadores;
	}
	
}
