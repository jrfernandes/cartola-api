package com.analistajunior.cartola.service.exceptions;

public class ObjectNotFountException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	public ObjectNotFountException(String msg) {
		super(msg);
	}

}
