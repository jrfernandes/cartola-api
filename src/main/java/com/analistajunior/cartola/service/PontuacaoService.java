package com.analistajunior.cartola.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.analistajunior.cartola.model.Pontuacao;
import com.analistajunior.cartola.repository.PontuacaoRepository;

@Service
public class PontuacaoService {

	@Autowired
	private PontuacaoRepository repository;
	
	@Autowired 
	private JogadorService jogadorService;

	public Pontuacao salvar(Pontuacao pontuacao) {
		jogadorService.buscarPorId(pontuacao.getJogador().getId());
		return repository.save(pontuacao);
	}
}
