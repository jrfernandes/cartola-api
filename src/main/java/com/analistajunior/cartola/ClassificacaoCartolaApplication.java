package com.analistajunior.cartola;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClassificacaoCartolaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClassificacaoCartolaApplication.class, args);
	}
}
