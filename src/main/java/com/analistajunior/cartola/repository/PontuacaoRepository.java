package com.analistajunior.cartola.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.analistajunior.cartola.model.Jogador;
import com.analistajunior.cartola.model.Pontuacao;

public interface PontuacaoRepository extends JpaRepository<Pontuacao, Long>{
	
	@Query("select sum(p.pontos) from Pontuacao p where p.jogador=:jogador")
	public Double pontuacaoPorJogador(@Param("jogador") Jogador jogador);
	
	@Query("select sum(p.pontos) from Pontuacao p where p.jogador=:jogador and p.rodada=:rodada")
	public Double pontuacaoJogadorPorRodada(@Param("jogador") Jogador jogador, @Param("rodada") int rodada);
	
	public List<Pontuacao> findByRodada(Integer rodada);

	
}
