package com.analistajunior.cartola.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.analistajunior.cartola.model.Jogador;

public interface JogadorRepository extends JpaRepository<Jogador, Long>{
	public Jogador findByTime(String time);
	
	public Jogador findByLogin(String login);
	
	@Query("FROM Jogador j order by j.pontuacao desc")
	@Transactional(readOnly=true)
	public List<Jogador> classificacao();
	
	public List<Jogador> findByTimeStartingWith(String time);
}
