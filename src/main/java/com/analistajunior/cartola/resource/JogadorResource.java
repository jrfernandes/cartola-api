package com.analistajunior.cartola.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.analistajunior.cartola.model.Jogador;
import com.analistajunior.cartola.service.JogadorService;

@RestController
@RequestMapping("/jogadores")
@CrossOrigin(origins = "http://localhost:4200")
public class JogadorResource {

	@Autowired
	private JogadorService service;

	@PostMapping
	public ResponseEntity<Jogador> salvar(@Valid @RequestBody Jogador jogador) {
		Jogador jogadorSalvo = service.salvar(jogador);
		return ResponseEntity.status(HttpStatus.CREATED).body(jogadorSalvo);

	}

	@DeleteMapping("/{id}")
	public void deletar(@PathVariable Long id) {
		service.deletarJogador(id);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Jogador> buscarPorId(@PathVariable Long id) {
		Jogador jogador = service.buscarPorId(id);
		return jogador != null ? ResponseEntity.ok(jogador) : ResponseEntity.notFound().build();
	}

	@GetMapping(params = "time")
	public ResponseEntity<Jogador> buscarPorTime(String time) {
		Jogador jogador = service.buscarPorTime(time);
		return jogador != null ? ResponseEntity.ok(jogador) : ResponseEntity.notFound().build();
	}

	@PutMapping("/{id}")
	public ResponseEntity<Jogador> atualizarPontuacao(@RequestBody double pontos, @PathVariable Long id) {
		Jogador jogador = service.atualizarPontuacao(id, pontos);
		return ResponseEntity.ok(jogador);
	}

	@PutMapping("/adicionar/{id}")
	public ResponseEntity<Jogador> adicionarPontuacao(@RequestBody double pontos, @PathVariable Long id) {
		Jogador jogador = service.adicionarPontuacao(id, pontos);
		return ResponseEntity.ok(jogador);
	}

	@GetMapping("/classificacao")
	public ResponseEntity<List<Jogador>> classificacaoGeral() {
		return ResponseEntity.ok(service.classificacaoGeral());
	}
	
	@GetMapping("/classificacao/{rodada}")
	public ResponseEntity<List<Jogador>> classificacaoRodada(@PathVariable int rodada){
		return ResponseEntity.ok(service.classificacaoRodada(rodada));
	}
	
	@GetMapping("/classificacao/mes/{mes}")
	public ResponseEntity<List<Jogador>> classificacaoMes(@PathVariable int mes){
		return ResponseEntity.ok(service.classificacaoMes(mes));
	}
}
