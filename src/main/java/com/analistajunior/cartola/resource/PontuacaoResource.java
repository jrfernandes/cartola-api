package com.analistajunior.cartola.resource;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.analistajunior.cartola.model.Pontuacao;
import com.analistajunior.cartola.service.PontuacaoService;

@RestController
@RequestMapping("/pontuacao")
public class PontuacaoResource {
	
	@Autowired
	private PontuacaoService service;
	
	
	@PostMapping
	public ResponseEntity<Pontuacao> salvar(@Valid @RequestBody Pontuacao pontuacao){
		Pontuacao pontuacaoSalva = service.salvar(pontuacao);
		return ResponseEntity.status(HttpStatus.CREATED).body(pontuacaoSalva);		
	}
	
}
